# Momentum Engine - 2D Semi-real Physics Platforming Engine
#
# Copyright (C) 2015 Robert Cochran
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License in the LICENSE file for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

import sys
import json

from momentum_engine import common
import momentum_engine.common.network

def send_join(server):
    common.network.send_packet(server, "join")
    resp, host = common.network.recv_packet()

    if not resp:
        return (False, "Response has no data")

    if resp["type"] == "failure":
        return (False, resp["msg"])
    elif resp["type"] != "success":
        return (False, "Got unexpected {!r} packet")

    return (True, "Success")

def get_boundary(server):
    common.network.send_packet(server, "retrieve", what = "bounds")
    resp, host = common.network.recv_packet()

    if not resp:
        return (None, "Response has no data")

    return (json.loads(resp["value"]), "Success")

def get_player_info(server, who = None):
    if not who:
        common.network.send_packet(server, "retrieve", what = "player")
    else:
        common.network.send_packet(server, "retrieve", what = "player",
            who = who)

    resp, host = common.network.recv_packet()

    if not resp:
        return (None, "Response has no data")

    return (json.loads(resp["value"]), "Success")

def update_player(server, what, new_value):
    common.network.send_packet(server, "update", value = {what : new_value})

def get_clients(server):
    common.network.send_packet(server, "clients")
    resp, host = common.network.recv_packet()

    if not resp:
        return (None, "Response has no data")

    return (json.loads(resp["value"]), "Success")
