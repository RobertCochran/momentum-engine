#!/usr/bin/env python3

# Momentum Engine - 2D Semi-real Physics Platforming Engine
#
# Copyright (C) 2015 Robert Cochran
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License in the LICENSE file for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# System imports
import sys
import json
import ctypes
from sdl2 import *

# Project imports
from momentum_engine import common
import momentum_engine.common.network
from . import network
from . import player
from . import fps

def run(args):
    server_addr = "localhost" if len(args) < 2 else args[1]

    if not common.network.init_client(server_addr, 12397):
        print("Could not connect to server!")
        sys.exit(-1)

    server = (server_addr, 12397)

    ok, err = network.send_join(server)

    if not ok:
        print("Couldn't join : {}. Bailing out...".format(err))
        sys.exit(-1)

    bounds, err = network.get_boundary(server)

    if not bounds:
        print("Couldn't get boundary info : {}. Bailing out...".format(err));
        sys.exit(-1)

    win_size = bounds[0], bounds[1]

    value, err = network.get_player_info(server)

    if not value:
        print("Couldn't get player info : {}. Bailing out...".format(err));

    my_player = player.Player(value["x"], value["y"], value["w"], value["h"])

    # List of keys that are down
    keys_down = []

    SDL_Init(SDL_INIT_VIDEO)

    window = SDL_CreateWindow(b"Momentum Engine Client", SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED, win_size[0], win_size[1], 0)
    window_render = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED)

    run = True
    event = SDL_Event()

    while run:
        frame_start = SDL_GetTicks()

        while SDL_PollEvent(ctypes.byref(event)):
            if event.type == SDL_QUIT:
                run = False
                break

            elif event.type == SDL_KEYDOWN:
                if event.key.keysym.sym == SDLK_w and "w" not in keys_down:
                    keys_down.append("w")
                    network.update_player(server, "ymovement",
                        my_player.ymovement - 1)

                elif event.key.keysym.sym == SDLK_s and "s" not in keys_down:
                    keys_down.append("s")
                    network.update_player(server, "ymovement",
                        my_player.ymovement + 1)

                elif event.key.keysym.sym == SDLK_a and "a" not in keys_down:
                    keys_down.append("a")
                    network.update_player(server, "xmovement",
                        my_player.xmovement - 1)

                elif event.key.keysym.sym == SDLK_d and "d" not in keys_down:
                    keys_down.append("d")
                    network.update_player(server, "xmovement",
                        my_player.xmovement + 1)

            elif event.type == SDL_KEYUP:
                if event.key.keysym.sym == SDLK_w and "w" in keys_down:
                    keys_down.remove("w")
                    if my_player.ymovement == 0: continue

                    network.update_player(server, "ymovement",
                        my_player.ymovement + 1)

                elif event.key.keysym.sym == SDLK_s and "s" in keys_down:
                    keys_down.remove("s")
                    if my_player.ymovement == 0: continue

                    network.update_player(server, "ymovement",
                        my_player.ymovement - 1)

                elif event.key.keysym.sym == SDLK_a and "a" in keys_down:
                    keys_down.remove("a")
                    if my_player.xmovement == 0: continue

                    network.update_player(server, "xmovement",
                        my_player.xmovement + 1)

                elif event.key.keysym.sym == SDLK_d and "d" in keys_down:
                    keys_down.remove("d")
                    if my_player.xmovement == 0: continue

                    network.update_player(server, "xmovement",
                        my_player.xmovement - 1)

        update_info, err = network.get_player_info(server)

        if not update_info: continue

        for k in update_info:
            my_player[k] = update_info[k]

        clients, err = network.get_clients(server)

        # Make a clean client object dictionary
        client_objs = {}

        if clients:
            for k in clients:
                client_dict, err = network.get_player_info(server, k)
                if not client_dict:
                    print("Error getting client object: {}".format(err))
                    continue
                client_objs[k] = player.Player(client_dict['x'],
                    client_dict['y'], client_dict['w'], client_dict['h'])

        # Render

        SDL_RenderClear(window_render)

        my_player.render(window_render)

        if clients:
            for v in client_objs.values():
                player.Player.render(v, window_render)

        SDL_RenderPresent(window_render)

        # Sleep to maintain 60 FPS if necessary
        if int(1000 / 60) > (SDL_GetTicks() - frame_start):
            SDL_Delay(int(1000 / 60) - (SDL_GetTicks() - frame_start))

        # Record frame time
        fps.add_frame((SDL_GetTicks() - frame_start) / 1000)

    common.network.send_packet(server, "leave")

    SDL_DestroyWindow(window)
    SDL_Quit()
