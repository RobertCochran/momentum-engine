#!/usr/bin/env python3

# Momentum Engine - 2D Semi-real Physics Platforming Engine
#
# Copyright (C) 2015 Robert Cochran
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License in the LICENSE file for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# System imports
import sys
import json

# Project imports
from momentum_engine import common
import momentum_engine.common.network
from . import player

world = {
    "bounds" : (800, 600),
    "player" : {"limit":5},
}

def run(args):
    common.network.init_server()

    print("Ready for connections")

    while True:
        # Handle incoming packets
        json_packet, host = common.network.recv_packet()

        if json_packet == "":
            # Timeout for remote host - just move on
            continue

        if json_packet is None:
            print("Packet is not valid JSON")
            continue

        if not "type" in json_packet:
            print("Packet has no 'type' object. Ignoring...")
            continue

        identifier = "{}:{}".format(*host)

        if json_packet["type"] == "join":
            # TODO : Only a single client is allowed.
            # Allow multiple concurrent clients
            if (len(world["player"]) - 1) == world["player"]["limit"]:
                common.network.send_packet(host, "failure",
                    msg = "Limit reached")
                continue
            elif not identifier in world["player"]:
                world["player"][identifier] = player.Player(375, 275, 25, 25)
                common.network.send_packet(host, "success")
                print("Added host {} to connection list".format(identifier))
            else:
                common.network.send_packet(host, "failure",
                    msg = "Already connected")
                print("Host {} is already connected".format(identifier))

        if not identifier in world["player"]:
            print("Recieved packet from unknown host {}. Ignoring.".format(identifier))
            continue

        if json_packet["type"] == "leave":
            del world["player"][identifier]
            common.network.send_packet(host, "success")
            print("Host {} disconnected".format(identifier))

        if json_packet["type"] == "retrieve":
            if not "what" in json_packet:
                common.network.send_packet(host, "failure",
                    msg = "No object specified")
                continue

            if not json_packet["what"] in world:
                common.network.send_packet(host, "failure",
                    msg = "No such value '{}'".format(json_packet["what"]))
                continue
            # Do a temporary special case check for Player class objects
            # TODO : Make it so that there is no need for special case check
            if json_packet["what"] == "player":
                if not "who" in json_packet: # Send the requesting player
                    common.network.send_packet(host, "retrieve",
                        value = json.dumps(world["player"][identifier].__dict__,
                        separators = (',',':')), what = json_packet["what"])
                else: # Send who was requested
                    who = json_packet["who"]

                    if who in world["player"]:
                        common.network.send_packet(host, "retrieve",
                            value = json.dumps(world["player"][who].__dict__,
                            separators = (',',':')), what = json_packet["what"])
            else:
                common.network.send_packet(host, "retrieve",
                    value = json.dumps(world[json_packet["what"]],
                    separators = (',',':')), what = json_packet["what"])

        if json_packet["type"] == "update":
            # Assume we are updating the host's player
            # TODO : This will need to be generalized for multiple players

            if not "value" in json_packet:
                print("No value given in 'update' packet")
                continue

            for k, v in json_packet["value"].items():
                world["player"][identifier][k] = v

        if json_packet["type"] == "clients":
            clients = list(world["player"].keys())
            # Do not send requester's identifier
            clients.remove(identifier)
            # Do not send useless limit key
            clients.remove("limit")

            common.network.send_packet(host, "clients",
                value = json.dumps(clients, separators = (',',':')))

        if identifier in world["player"]:
            world["player"][identifier].update(world["bounds"])
